# Sudoku_Solver
Accepts input in the form of a png file of the grid, and solves it using sudokuImageMaker and sudokuSolver

# Files
* sudokuSolver solves a sudoku puzzle when input is given in the form of a 9x9 array
* sudokuImageMaker interprets the image by comparing each cell against the images in the \"Numbers\" folder

# How to Run
* First, navigate to this folder after cloning or downloading the repo onto your device. 
* Install the required modules by running `pip install -r requirements.txt`
* Make sure your image is in the same folder.
* Then, simply run sudokuImageMaker using `python3 sudokuImageMaker.py`
